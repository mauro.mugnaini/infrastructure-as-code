# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    graphdb-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["graphdb-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Graphdb graphdb-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    graphdb-test-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["graphdb-test-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Graphdb graphdb-test-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    aggregator-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["aggregator-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Aggregator aggregator-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    opensearch-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["opensearch-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "OpenSearch opensearch-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    opensearch-test-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["opensearch-test-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "OpenSearch opensearch-test-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    solr-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["solr-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Solr solr-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
