# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    geonetwork_ariadne = {
      name              = "geonetwork-ariadne",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_ariadne_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_fisherieatlas = {
      name              = "geonetwork-fisherieatlas",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_fisherieatlas_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_grsf = {
      name              = "geonetwork-grsf",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_grsf_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_iotcss3 = {

      name              = "geonetwork-iotcss3",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_iotcss3_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_sdilab = {
      name              = "geonetwork-sdilab",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_sdilab_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_spatialdata = {
      name              = "geonetwork-spatialdata",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_spatialdata_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_wecafcfirms = {
      name              = "geonetwork-wecafcfirms",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_wecafcfirms_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_bluecloud = {
      name              = "geonetwork-bluecloud",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_bluecloud_data_volume",
        size   = "30",
        device = "/dev/vdb"
      }
    },
    geonetwork_tunaatlas = {
      name              = "geonetwork-tunaatlas",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_tunaatlas_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_globalfisheriesatlas = {
      name              = "geonetwork-globalfisheriesatlas",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_globalfisheriesatlas_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_prod = {
      name              = "geonetwork-prod",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_prod_data_volume",
        size   = "40",
        device = "/dev/vdb"
      }
    },
    geonetwork_itineris_carbon = {
      name              = "geonetwork-itineris-carbon",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_itineris_carbon_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    geonetwork-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-fisherieatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-fisherieatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-fisherieatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-grsf = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-grsf", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-grsf"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-iotcss3 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-iotcss3", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-iotcss3"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-sdilab = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-sdilab", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-sdilab"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-spatialdata = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-spatialdata", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-spatialdata"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-wecafcfirms = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-wecafcfirms", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-wecafcfirms"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-bluecloud = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-bluecloud", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-bluecloud"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-tunaatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-tunaatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-tunaatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-globalfisheriesatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-globalfisheriesatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-globalfisheriesatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-prod = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-prod", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-prod"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-itineris-carbon = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-itineris-carbon", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-itineris-carbon"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
