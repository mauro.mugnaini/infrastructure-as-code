# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

locals {
  cname_target = "swarm-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# We only manage the DNS records, for the gCore services
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    resource-manager-openaire = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resource-manager-openaire", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resource-manager-openaire"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-gcubeapps = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-gcubeapps", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-gcubeapps"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-farm = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-farm", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-farm"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-d4research = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-d4research", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-d4research"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-d4os = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-d4os", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-d4os"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-parthenosvo = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-parthenosvo", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-parthenosvo"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-sobigdata = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-sobigdata", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-sobigdata"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    resourcemanager-root = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["resourcemanager-root", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore resourcemanager-root"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    vremodeler-gcubeapps = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["vremodeler-gcubeapps", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore vremodeler-gcubeapps"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    vremodeler-d4research = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["vremodeler-d4research", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore vremodeler-d4research"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    vremodeler-all = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["vremodeler-all", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "gCore vremodeler-all"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
