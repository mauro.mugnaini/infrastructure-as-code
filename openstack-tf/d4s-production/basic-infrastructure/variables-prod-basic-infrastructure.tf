variable "dns_zone_id" {
  default             = "74135b34-1a9c-4c01-8cf0-22450a5660c4"
}

variable "main_private_network_id" {
  default = "020df98d-ae72-452a-b376-3b6dc289acac"
}

variable "main_private_subnet_id" {
  default = "5d7b83ad-e058-4a3a-bfd8-d20ba6d42e1a"
}