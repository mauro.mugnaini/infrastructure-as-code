# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

module "timescaledb" {
  source = "../../modules/timescaledb"

  timescaledb_server_data = {
    node_name             = "timescaledb"
    node_data_disk_size   = 30
    node_data_disk_device = "/dev/vdb"
  }

  timescaledb_nodes_count = 2
  timescaledb_node_flavor = "m1.large"
  timescaledb_ip          = ["192.168.11.5", "192.168.11.6"]
}
