# NFS shares required by 
# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "webodv_private_data" {
  name        = "sobigdata_webodv_private_data"
  description = "NFS share for the sobigdata webodv private data"
  share_proto = "NFS"
  size        = 1000
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "webodv_mgr_private_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_private_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "webodv_workers_private_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_private_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "webodv_home_data" {
  name        = "sobigdata_webodv_home_data"
  description = "NFS share for the sobigdata webodv home data"
  share_proto = "NFS"
  size        = 10
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "webodv_mgr_home_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_home_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "webodv_workers_home_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_home_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "webodv_public_data" {
  name        = "sobigdata_webodv_home_data"
  description = "NFS share for the sobigdata webodv home data"
  share_proto = "NFS"
  size        = 10
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "webodv_mgr_public_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_public_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "webodv_workers_public_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_public_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "webodv_nfs_private_data" {
  value = openstack_sharedfilesystem_share_v2.webodv_private_data
}

output "webodv_nfs_private_data_mgr_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_mgr_private_data_share_access
  sensitive = true
}

output "webodv_nfs_private_data_workers_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_workers_private_data_share_access
  sensitive = true
}

output "webodv_nfs_home_data" {
  value = openstack_sharedfilesystem_share_v2.webodv_home_data
}

output "webodv_nfs_home_data_mgr_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_mgr_home_data_share_access
  sensitive = true
}

output "webodv_nfs_home_data_workers_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_workers_home_data_share_access
  sensitive = true
}

output "webodv_nfs_public_data" {
  value = openstack_sharedfilesystem_share_v2.webodv_public_data
}

output "webodv_nfs_public_data_mgr_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_mgr_public_data_share_access
  sensitive = true
}

output "webodv_nfs_public_data_workers_acls" {
  value = openstack_sharedfilesystem_share_access_v2.webodv_workers_public_data_share_access
  sensitive = true
}
