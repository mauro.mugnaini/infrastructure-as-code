# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}


module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    geoserver_geona = {
      name              = "geoserver-geona",
      description       = "Geoserver geona instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_geona_data_volume",
        size   = "50",
        device = "/dev/vdb"
      }
    },
    geoserver_esquiline = {
      name              = "geoserver-esquiline",
      description       = "Geoserver esquiline instance",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_esquiline_data_volume",
        size   = "20",
        device = "/dev/vdb",
      }
    },
    geoserver_ariadne = {
      name              = "geoserver-ariadne",
      description       = "Geoserver ariadne instance",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_ariadne_data_volume",
        size   = "20",
        device = "/dev/vdb",
      }
    },
    geoserver_grsf = {
      name              = "geoserver-grsf",
      description       = "Geoserver grsf instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geoserver_grsf_data_volume"
        size   = "40",
        device = "/dev/vdb",
      }
    },
    geoserver_protectedareaimpactmaps = {
      name              = "geoserver-protectedareaimpactmaps",
      description       = "Geoserver protectedareaimpactmaps instance",
      flavor            = module.common_variables.flavor_list.c1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_protectedareaimpactmaps_data_volume"
        size   = "70",
        device = "/dev/vdb",
      }
    },
    geoserver_sdilab = {
      name              = "geoserver-sdilab",
      description       = "Geoserver sdilab instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_sdilab_data_volume",
        size   = "100",
        device = "/dev/vdb",
      }
    },
    geoserver_tunaatlas = {
      name              = "geoserver-tunaatlas",
      description       = "Geoserver tunaatlas instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_tunaatlas_data_volume",
        size   = "60",
        device = "/dev/vdb",
      }
    },
    geoserver_wecafcfirms = {
      name              = "geoserver-wecafcfirms",
      description       = "Geoserver wecafcfirms instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_wecafcfirms_data_volume",
        size   = "40",
        device = "/dev/vdb",
      }
    },
    geoserver_aquacultureatlas = {
      name              = "geoserver-aquacultureatlas",
      description       = "Geoserver aquacultureatlas instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_aquacultureatlas_data_volume",
        size   = "50",
        device = "/dev/vdb",
      }
    },
    geoserver_globalfisheriesatlas = {
      name              = "geoserver-globalfisheriesatlas",
      description       = "Geoserver globalfisheriesatlas instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_globalfisheriesatlas_data_volume",
        size   = "50",
        device = "/dev/vdb",
      }
    },
    geoserver_marineenvironmentalindicators = {
      name              = "geoserver-marineenvironmentalindicators",
      description       = "Geoserver marineenvironmentalindicators instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_marineenvironmentalindicators_data_volume",
        size   = "50",
        device = "/dev/vdb",
      }
    },
    geoserver_itineris_carbon = {
      name              = "geoserver-itineris-carbon",
      description       = "Geoserver itineris carbon instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      volume = {
        name   = "geoserver_itineris_carbon_data_volume",
        size   = "40",
        device = "/dev/vdb",
      }
    }
  }
}


locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    geoserver-geona = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-geona", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-geona"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-esquiline = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-esquiline", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-esquiline"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-ariadne = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-ariadne", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-ariadne"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-grsf = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-grsf", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-grsf"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-protectedareaimpactmaps = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-protectedareaimpactmaps", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-protectedareaimpactmaps"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-sdilab = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-sdilab", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-sdilab"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-tunaatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-tunaatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-tunaatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-wecafcfirms = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-wecafcfirms", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-wecafcfirms"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-aquacultureatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-aquacultureatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-aquacultureatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-globalfisheriesatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-globalfisheriesatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geoserver-globalfisheriesatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-marineenvironmentalindicators = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-marineenvironmentalindicators", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-marineenvironmentalindicators"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geoserver-itineris-carbon = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geoserver-itineris-carbon", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geoserver geoserver-itineris-carbon"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
