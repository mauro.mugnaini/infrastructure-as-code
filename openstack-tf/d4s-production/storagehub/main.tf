# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_blockstorage_volume_v3" "storagehub_prod_data_volume" {
  count = 2
  name  = format("storagehub%01d_data_volume", count.index + 1)
  size  = "120"
}

resource "openstack_blockstorage_volume_v3" "storagehub_prod_temp_volume" {
  count = 2
  name  = format("storagehub%01d_temp_volume", count.index + 1)
  size  = "80"
}

# Instances
resource "openstack_compute_instance_v2" "storagehub1_prod" {
  name                    = "storagehub1"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.m1_xlarge
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 40
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql])
    content {
      name = network.value
    }
  }


  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_instance_v2" "storagehub2_prod" {
  name                    = "storagehub2"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.m1_xlarge
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 40
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql])
    content {
      name = network.value
    }
  }


  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}



resource "openstack_compute_volume_attach_v2" "storagehub1_attach_1" {
  instance_id = openstack_compute_instance_v2.storagehub1_prod.id
  volume_id   = openstack_blockstorage_volume_v3.storagehub_prod_data_volume.0.id
  device      = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "storagehub1_attach_2" {
  instance_id = openstack_compute_instance_v2.storagehub1_prod.id
  volume_id   = openstack_blockstorage_volume_v3.storagehub_prod_temp_volume.0.id
  device      = "/dev/vdc"

  depends_on = [openstack_compute_volume_attach_v2.storagehub1_attach_1]
}

resource "openstack_compute_volume_attach_v2" "storagehub2_attach_1" {
  instance_id = openstack_compute_instance_v2.storagehub2_prod.id
  volume_id   = openstack_blockstorage_volume_v3.storagehub_prod_data_volume.1.id
  device      = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "storagehub2_attach_2" {
  instance_id = openstack_compute_instance_v2.storagehub2_prod.id
  volume_id   = openstack_blockstorage_volume_v3.storagehub_prod_temp_volume.1.id
  device      = "/dev/vdc"

  depends_on = [openstack_compute_volume_attach_v2.storagehub2_attach_1]
}



locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    storagehub1 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["storagehub1", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "StorageHub1"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    storagehub2 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["storagehub2", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "StorageHub2"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    workspace_repo = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["workspace-repository", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Workspace repository (storagehub)"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
