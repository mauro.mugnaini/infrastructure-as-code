octavia_swarm_data = {
    swarm_lb_name = "l4-swarm-dev"
    swarm_lb_description = "L4 balancer that serves the D4Science DEV Docker Swarm cluster"
    octavia_flavor = "octavia_amphora-mvcpu-ha"
    octavia_flavor_id = "394988b5-6603-4a1e-a939-8e177c6681c7"
    swarm_lb_hostname = "swarm-lb"
    swarm_octavia_main_ip = "10.1.31.70"
    swarm_octavia_main_cidr = "10.1.31.70/32"
    # The following aren't available when the module runs so we have to get them with the command
    # openstack --os-cloud d4s-pre port list -f value | grep octavia-lb-vrrp
    # This means that the execution will fail
    octavia_vrrp_ip_1 = "10.1.31.246/32"
    octavia_vrrp_ip_2 = "10.1.31.69/32"
}

docker_swarm_data = {
    mgr_name = "swarm-mgr"
    mgr1_ip = "10.1.29.205"
    mgr1_cidr = "10.1.29.205/32"
    mgr2_ip = "10.1.30.212"
    mgr2_cidr = "10.1.30.212/32"
    mgr3_ip = "10.1.30.206"
    mgr3_cidr = "10.1.30.206/32"
    mgr_count = 3
    mgr_flavor = "m1.large"
    mgr_data_disk_size = 100
    worker_name = "swarm-worker"
    worker_count = 5
    worker_flavor = "m1.xlarge"
    worker_data_disk_size = 100
    nfs_server_name = "swarm-nfs-server"
    nfs_server_flavor = "m1.medium"
    nfs_server_data_disk_name = "Swarm NFS server data Disk"
    nfs_server_data_disk_size = 100
    nfs_server_data_disk_device = "/dev/vdb"
}

swarm_managers_ip =  ["10.1.29.205", "10.1.30.212", "10.1.30.206"]
