# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#
# Creates the server group "keycloak"
# Even in dev because this service is crucial the server group is
# created with anti-affinity policy
#
resource "openstack_compute_servergroup_v2" "keycloak_server_group" {
  name     = "keycloak"
  policies = [module.common_variables.policy_list.anti_affinity]
}

# Creating object bucket to store avatars
resource "openstack_objectstorage_container_v1" "keycloak_1" {
  name = "keycloak"
}

module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    keycloak = {
      name              = "keycloak",
      description       = "This instance serves keycloak service",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.default, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204
    }
  }
}

resource "openstack_dns_recordset_v2" "keycloak_dev_dns_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = join(".", ["accounts"], [data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
  description = "Keycloak d4science dev endpoint"
  ttl         = 8600
  type        = "CNAME"
  records     = [join(".", ["main-lb"], [data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])]
}
