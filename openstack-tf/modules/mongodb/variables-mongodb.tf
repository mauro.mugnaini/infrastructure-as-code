variable "mongodb_cluster_data" {
  type = map(string)
  default = {
    count           = 3
    name            = "mongodb-replica"
    flavor          = "m1.small"
    data_disk_size  = 100
    image_type_name = "Ubuntu-Focal-20.04"
    image_type_uuid = "75c23040-2be7-49e9-8029-a16dc9f755d1"
  }
}

variable "mongodb_ip" {
  type    = list(string)
  default = []

}

variable "mongodb_vol_data" {
  type = map(string)
  default = {
    name            = "mongodb-vol"
    flavor          = "m1.small"
    data_disk_size  = 100
    image_type_name = "Ubuntu-Focal-20.04"
    image_type_uuid = "75c23040-2be7-49e9-8029-a16dc9f755d1"
  }
}

variable "mongodb_vol_ip" {
  default = ""
}
