#
# Server groups for both the masters and the workers
#
resource "openstack_compute_servergroup_v2" "mongodb" {
  name     = "mongodb"
  policies = ["anti-affinity"]
}
#
# Security groups
#
# Rules
# 80 from 0/0
# 9101 from prometheus
# 27017 da: garr-ct1, garr-na, garr-pa1, InfraScience, S2I2S
resource "openstack_networking_secgroup_v2" "mongodb_cluster_traffic" {
  name                 = "mongodb_cluster_traffic"
  delete_default_rules = "true"
  description          = "Traffic between the MongoDB nodes"
}
resource "openstack_networking_secgroup_rule_v2" "access_to_the_mongodb_service_from_the_internal_network" {
  security_group_id = openstack_networking_secgroup_v2.mongodb_cluster_traffic.id
  description       = "Access to the MongoDB service"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 27017
  port_range_max    = 27017
  remote_ip_prefix  = var.main_private_subnet.cidr
}
resource "openstack_networking_secgroup_rule_v2" "access_to_the_mongodb_service_from_the_outside" {
  for_each          = toset([var.networks_with_d4s_services.infrascience_net, var.networks_with_d4s_services.s2i2s_net, var.networks_with_d4s_services.garr_ct1_net, var.networks_with_d4s_services.garr_pa1_net, var.networks_with_d4s_services.garr_na_net, var.networks_with_d4s_services.isti_net])
  security_group_id = openstack_networking_secgroup_v2.mongodb_cluster_traffic.id
  description       = "Access to the MongoDB service"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 27017
  port_range_max    = 27017
  remote_ip_prefix  = each.value
}
resource "openstack_networking_secgroup_rule_v2" "mongodb_plain_http_for_letsencrypt" {
  security_group_id = openstack_networking_secgroup_v2.mongodb_cluster_traffic.id
  description       = "Plain HTTP for letsencrypt"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "mongodb_prometheus_exporter" {
  security_group_id = openstack_networking_secgroup_v2.mongodb_cluster_traffic.id
  description       = "Prometheus exporter for MongoDB"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9101
  port_range_max    = 9101
  remote_ip_prefix  = var.basic_services_ip.prometheus_cidr
}

#
# Mongodb cluster VMs
#
# Instance
resource "openstack_compute_instance_v2" "mongodb_cluster_nodes" {
  count                   = var.mongodb_cluster_data.count
  name                    = format("%s-%02d", var.mongodb_cluster_data.name, count.index + 2)
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.mongodb_cluster_data.flavor
  key_pair                = module.ssh_settings.ssh_key_file
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.mongodb_cluster_traffic.name]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.mongodb.id
  }
  block_device {
    uuid                  = var.mongodb_cluster_data.image_type_uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.mongodb_cluster_data.data_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = var.main_private_network.name
    fixed_ip_v4 = var.mongodb_ip.* [count.index]
  }
  user_data = file("${module.common_variables.ubuntu_2204.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "mongodb_cluster_floating_ip" {
  count = var.mongodb_cluster_data.count
  pool  = var.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  # dns_name = "main-lb"
  # dns_domain = var.dns_zone.zone_name
  description = format("MongoDB cluster node %s-%02d", var.mongodb_cluster_data.name, count.index + 2)
}

resource "openstack_compute_floatingip_associate_v2" "mongodb_cluster_ip" {
  count       = var.mongodb_cluster_data.count
  floating_ip = element(openstack_networking_floatingip_v2.mongodb_cluster_floating_ip.*.address, count.index)
  instance_id = element(openstack_compute_instance_v2.mongodb_cluster_nodes.*.id, count.index)
  depends_on  = [openstack_networking_floatingip_v2.mongodb_cluster_floating_ip]
}

resource "openstack_dns_recordset_v2" "mongodb_cluster_dns_recordsets" {
  count       = var.mongodb_cluster_data.count
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = join(".", [element(openstack_compute_instance_v2.mongodb_cluster_nodes.*.name, count.index), data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
  description = "Mongodb public hostnames"
  ttl         = 8600
  type        = "A"
  records     = [element(openstack_networking_floatingip_v2.mongodb_cluster_floating_ip.*.address, count.index)]
  depends_on  = [openstack_networking_floatingip_v2.mongodb_cluster_floating_ip]
}

#
# MongoDB vol node
#
# Instance
resource "openstack_compute_instance_v2" "mongodb_vol_node" {
  name                    = "mongodb-vol"
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.mongodb_vol_data.flavor
  key_pair                = module.ssh_settings.ssh_key_file
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.mongodb_cluster_traffic.name]
  block_device {
    uuid                  = var.mongodb_vol_data.image_type_uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.mongodb_vol_data.data_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = var.main_private_network.name
    fixed_ip_v4 = var.mongodb_vol_ip
  }
  user_data = file("${module.common_variables.ubuntu_2204.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "mongodb_vol_floating_ip" {
  pool = var.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  # dns_name = "main-lb"
  # dns_domain = var.dns_zone.zone_name
  description = "MongoDB Volatile"
}

resource "openstack_compute_floatingip_associate_v2" "mongodb_vol_public_ip" {
  floating_ip = openstack_networking_floatingip_v2.mongodb_vol_floating_ip.address
  instance_id = openstack_compute_instance_v2.mongodb_vol_node.id
  depends_on  = [openstack_networking_floatingip_v2.mongodb_vol_floating_ip]
}

resource "openstack_dns_recordset_v2" "mongodb_vol_dns_recordsets" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = join(".", [openstack_compute_instance_v2.mongodb_vol_node.name], [data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
  description = "Mongodb Volatile public hostnames"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.mongodb_vol_floating_ip.address]
  depends_on  = [openstack_networking_floatingip_v2.mongodb_vol_floating_ip]
}
