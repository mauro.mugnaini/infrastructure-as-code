output "liferay_data" {
  value = var.liferay_data
}

output "liferay_ip_addrs" {
  value = var.liferay_ip_addrs
}

output "liferay_recordsets" {
  value = var.liferay_recordsets
}

output "nfs_port_data" {
  value = openstack_compute_interface_attach_v2.nfs_port_to_liferay
}

output "liferay_nfs_volume_data" {
  value = openstack_sharedfilesystem_share_v2.liferay_static
}

output "liferay_nfs_volume_acls" {
  value = openstack_sharedfilesystem_share_access_v2.liferay_nfs_share_access
  sensitive = true
}
