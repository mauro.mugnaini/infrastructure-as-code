# Global definitions
variable "main_region" {
  type    = string
  default = "isti_area_pi_1"
}

variable "external_network" {
  type = map(string)
  default = {
    name = "external-network"
    id   = "1d2ff137-6ff7-4017-be2b-0d6c4af2353b"
  }
}

variable "floating_ip_pools" {
  type = map(string)
  default = {
    main_public_ip_pool = "external-network"
  }
}

variable "resolvers_ip" {
  type    = list(string)
  default = ["146.48.29.97", "146.48.29.98", "146.48.29.99"]
}

variable "mtu_size" {
  type    = number
  default = 8942
}

variable "availability_zones_names" {
  type = map(string)
  default = {
    availability_zone_no_gpu   = "cnr-isti-nova-a"
    availability_zone_with_gpu = "cnr-isti-nova-gpu-a"
  }
}

variable "ubuntu_1804" {
  type = map(string)

  default = {
    name           = "Ubuntu-Bionic-18.04"
    uuid           = "7ed6a2cd-2b07-482e-8ce4-f018dff16c89"
    user_data_file = "../../openstack_vm_data_scripts/ubuntu1804.sh"
  }
}

variable "ubuntu_2204" {
  type = map(string)
  default = {
    name           = "Ubuntu-Jammy-22.04"
    uuid           = "54768889-8556-4be4-a2eb-82a4d9b34627"
    user_data_file = "../../openstack_vm_data_scripts/ubuntu2204.sh"
  }
}

variable "centos_7" {
  type = map(string)
  default = {
    name = "CentOS-7"
    uuid = "f0187a99-64f6-462a-ab5f-ef52fe62f2ca"
  }
}

variable "almalinux_9" {
  type = map(string)
  default = {
    name = "AlmaLinux-9.0-20220718"
    uuid = "541650fc-dd19-4f38-bb1d-7333ed9dd688"
  }
}

variable "ubuntu1804_data_file" {
  default = "../../openstack_vm_data_scripts/ubuntu1804.sh"
}

variable "ubuntu2204_data_file" {
  default = "../../openstack_vm_data_scripts/ubuntu2204.sh"
}

variable "el7_data_file" {
  default = "../../openstack_vm_data_scripts/el7.sh"
}

variable "ssh_jump_proxy" {
  type = map(string)
  default = {
    name   = "ssh-jump-proxy"
    flavor = "m2.small"
  }
}

variable "internal_ca_data" {
  type = map(string)
  default = {
    name   = "ca"
    flavor = "m1.small"
  }
}

variable "prometheus_server_data" {
  type = map(string)
  default = {
    name                       = "prometheus"
    flavor                     = "m1.medium"
    vol_data_name              = "prometheus-data"
    vol_data_size              = "100"
    vol_data_device            = "/dev/vdb"
    public_grafana_server_cidr = "146.48.28.103/32"
  }
}

variable "shared_postgresql_server_data" {
  type = map(string)
  default = {
    name            = "shared-postgresql-server"
    flavor          = "m1.medium"
    vol_data_name   = "shared-postgresql-data"
    vol_data_size   = "100"
    vol_data_device = "/dev/vdb"
    #    vol_backup_name = ""
    #    vol_backup_size = ""
    #    vol_backup_device = ""
    network_name          = "postgresql-srv-net"
    network_description   = "Network used to communicate with the shared postgresql service"
    network_cidr          = "192.168.0.0/22"
    allocation_pool_start = "192.168.0.100"
    allocation_pool_end   = "192.168.3.254"
    server_ip             = "192.168.0.5"
    server_cidr           = "192.168.0.5/22"
  }
}

variable "haproxy_l7_data" {
  type = map(string)
  default = {
    name      = "main-haproxy-l7"
    haproxy_1 = "haproxy-l7-1"
    haproxy_2 = "haproxy-l7-2"
    flavor    = "m1.medium"
    vm_count  = "2"
  }
}

variable "resource_registry_addresses" {
  type = map(string)
  default = {
  }
}

variable "smartexecutor_addresses" {
  type = map(string)
  default = {
  }
}

# Added by Francesco
# Create in the path 'modules/ssh-key-ref' the file 'ssh-key-ref-outputs.tf'
# with the following outputs:

# output "ssh_key_file" {
#   value     = "~/.ssh/{YOUR_PRIVATE_KEYNAME}"
#   sensitive = true
# }

# output "ssh_key_name" {
#   value     = "{YOUR_KEYNAME}"
#   sensitive = false
# }

# Then you can use above outputs in your 'file.tf' (if it contains the soft link to variables.tf) as:
#  module.ssh_settings.ssh_key_file
#  module.ssh_settings.ssh_key_name

# e.g.
# 
# resource "openstack_compute_instance_v2" "geoserver" {
#   key_pair                = module.ssh_settings.ssh_key_name

# Added by Francesco
variable "flavor_list" {
  type = map(string)
  default = {
    c1_small  = "c1.small"  #RAM 2 - VCPUs 2
    c1_medium = "c1.medium" #RAM 4 - VCPUs 4
    c1_large  = "c1.large"  #RAM 8 - VCPUs 8
    c2_large  = "c2.large"  #RAM 16 - VCPUs 16 	
    d1_medium = "d1.medium" #RAM 12 - VCPUs	2	
    d1_large  = "d1.large"  #RAM 24	- VCPUs 4	
    m1_medium = "m1.medium" #RAM 4 - VCPUs 2
    m1_large  = "m1.large"  #RAM 8 - VCPUs 4
    m1_xlarge = "m1.xlarge" #RAM 16 - VCPUs 8
    m1_xxl    = "m1.xxl"    #RAM 32 - VCPUS 16
    m2_small  = "m2.small"  #RAM 8 - VCPUs 2
    m2_medium = "m2.medium" #RAM 16 - VCPUs 4
    m2_large  = "m2.large"  #RAM 32 - VCPUs 8
    m3_large  = "m3.large"  #RAM 64 - VCPUs 16	
  }
}

# Added by Francesco
variable "security_group_list" {
  type = map(string)
  default = {
    postgreSQL                             = "PostgreSQL service"
    acaland                                = "acaland's dev machine"
    haproxy                                = "traffic_from_main_lb_to_haproxy_l7"
    access_to_orientdb                     = "access_to_orientdb"
    dataminer-publish                      = "dataminer-publish"
    docker_swarm_NFS                       = "Docker Swarm NFS"
    public_HTTPS                           = "Public HTTPS"
    orientdb_internal_docker_traffic       = "orientdb_internal_docker_traffic"
    limited_SSH_access                     = "Limited SSH access"
    debugging_from_jump_node               = "debugging_from_jump_node"
    access_to_the_timescaledb_service      = "access_to_the_timescaledb_service"
    docker_swarm                           = "Docker Swarm"
    http_and_https_from_the_load_balancers = "traffic_from_the_main_load_balancers"
    limited_HTTPS_access                   = "restricted_web_service"
    mongo                                  = "mongo"
    limited_SSH_access                     = "Limited SSH access"
    default                                = "default"
    cassandra                              = "Cassandra"
    access_to_orientdb_se                  = "access_to_orientdb_se"
    nfs_share_no_ingress                   = "nfs_share_no_ingress"
  }
}

# Added by Francesco
variable "policy_list" {
  type = map(string)
  default = {
    soft_anti_affinity = "soft-anti-affinity"
    anti_affinity      = "anti-affinity"
    affinity           = "affinity"
    soft_affinity      = "soft-affinity"
  }
}

variable "networks_list" {
  type = map(string)
  default = {
    shared_postgresql = "postgresql-srv-net"
    swarm             = "swarm-nfs-net"
    timescaledb       = "timescaledb-net"
    orientdb          = "orientdb-net"
    orientdb_se       = "orientdb-se-net"
    cassandra         = "cassandra-net"
  }
}

variable "ssh_sources" {
  type = map(string)
  default = {
    s2i2s_vpn_1_cidr      = "146.48.28.10/32"
    s2i2s_vpn_2_cidr      = "146.48.28.11/32"
    d4s_vpn_1_cidr        = "146.48.122.27/32"
    d4s_vpn_2_cidr        = "146.48.122.49/32"
    shell_d4s_cidr        = "146.48.122.95/32"
    infrascience_net_cidr = "146.48.122.0/23"
  }
}

variable "networks_with_d4s_services" {
  type = map(string)
  default = {
    "isti_net"         = "146.48.80.0/21"
    "s2i2s_net"        = "146.48.28.0/22"
    "infrascience_net" = "146.48.122.0/23"
    "garr_ct1_net"     = "90.147.166.0/23"
    "garr_pa1_net"     = "90.147.188.0/23"
    "garr_na_net"      = "90.147.152.0/24"
  }
}

# variable "default_security_group_name" {
#   default = "default_for_all"
# }

