
#Default geoserver vol_data_device
variable "geo_instance_basic_data" {
  type = map(string)
  default = {
    vol_data_device = "/dev/vdb"
  }
}

#Default geo_service_instances_map is EMPTY. Override it to create a proper geoserver plan
variable "geo_service_instances_map" {
  type = map(object({
    name              = string
    description       = string
    flavor            = string
    networks          = list(string)
    security_groups   = list(string)
    server_groups_ids = list(string)
    image_ref         = map(string)
    vol_data_name     = string
    vol_data_size     = string
  }))
  default = {
    geoserver = { name = "", description = "", flavor = "", networks = [], security_groups = [], server_groups_ids = [], image_ref = {}, vol_data_name = "", vol_data_size = "" }
  }

}
