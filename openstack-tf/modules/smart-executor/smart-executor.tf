# Accounting dashboard harvester
resource "openstack_compute_instance_v2" "accounting_dashboard_harvester" {
  name                    = var.accounting_dashboard_harvester.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.accounting_dashboard_harvester.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.networks_list.orientdb_se
  }
  network {
    name = var.networks_list.shared_postgresql
  }
  network {
    name = var.networks_list.timescaledb
  }
  user_data = file("${var.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Resource checker
resource "openstack_compute_instance_v2" "resource_checker" {
  name                    = var.resource_checker.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.resource_checker.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.networks_list.orientdb_se
  }
  user_data = file("${var.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Social data indexer
resource "openstack_compute_instance_v2" "social_data_indexer" {
  name                    = var.social_data_indexer.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.social_data_indexer.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.networks_list.orientdb_se
  }
  user_data = file("${var.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Accounting insert storage
resource "openstack_compute_instance_v2" "accounting_insert_storage" {
  name                    = var.accounting_insert_storage.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.accounting_insert_storage.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.networks_list.orientdb_se
  }
  user_data = file("${var.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Accounting aggregator
resource "openstack_compute_instance_v2" "accounting_aggregator" {
  name                    = var.accounting_aggregator.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.accounting_aggregator.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.networks_list.orientdb_se
  }
  user_data = file("${var.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}
