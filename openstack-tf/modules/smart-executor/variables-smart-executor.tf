variable "accounting_dashboard_harvester" {
  type = map(string)
  default = {
    name        = "accounting-dashboard-harvester-se-plugin"
    description = "Accounting Dashboard Harvester SE plugin"
    flavor      = "m1.medium"
  }
}

variable "resource_checker" {
  type = map(string)
  default = {
    name        = "resource-checker-se-plugin"
    description = "Resource checker SE plugin"
    flavor      = "c1.small"
  }
}

variable "social_data_indexer" {
  type = map(string)
  default = {
    name        = "social-data-indexer-se-plugin"
    description = "Social data indexer SE plugin"
    flavor      = "c1.small"
  }
}

variable "accounting_insert_storage" {
  type = map(string)
  default = {
    name        = "accounting-insert-storage-se-plugin"
    description = "Accounting insert storage SE plugin"
    flavor      = "c1.small"
  }
}

variable "accounting_aggregator" {
  type = map(string)
  default = {
    name        = "accounting-aggregator-se-plugin"
    description = "Accounting aggregator SE plugin"
    flavor      = "m1.medium"
  }
}
