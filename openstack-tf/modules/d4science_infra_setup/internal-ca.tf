resource "openstack_compute_instance_v2" "internal_ca" {
  name                    = var.internal_ca_data.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.internal_ca_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name]
  block_device {
    uuid                  = var.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = var.main_private_network.name
    fixed_ip_v4 = var.basic_services_ip.ca
  }
  user_data = file("${var.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}
