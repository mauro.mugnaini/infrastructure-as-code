# PostgreSQL shared server
# Network
resource "openstack_networking_network_v2" "shared_postgresql_net" {
  name                  = var.shared_postgresql_server_data.network_name
  admin_state_up        = "true"
  external              = "false"
  description           = var.shared_postgresql_server_data.network_description
  dns_domain            = var.dns_zone.zone_name
  mtu                   = var.mtu_size
  port_security_enabled = true
  shared                = false
  region                = var.main_region
}

# Subnet
resource "openstack_networking_subnet_v2" "shared_postgresql_subnet" {
  name            = "shared-postgresql-subnet"
  description     = "subnet used to connect to the shared PostgreSQL service"
  network_id      = openstack_networking_network_v2.shared_postgresql_net.id
  cidr            = var.shared_postgresql_server_data.network_cidr
  dns_nameservers = var.resolvers_ip
  ip_version      = 4
  enable_dhcp     = true
  no_gateway      = true
  allocation_pool {
    start = var.shared_postgresql_server_data.allocation_pool_start
    end   = var.shared_postgresql_server_data.allocation_pool_end
  }
}

# Security group
resource "openstack_networking_secgroup_v2" "shared_postgresql_access" {
  name                 = "access_to_the_shared_postgresql_service"
  delete_default_rules = "true"
  description          = "Access the shared PostgreSQL service using the dedicated network"
}

resource "openstack_networking_secgroup_rule_v2" "shared_postgresql_access_from_dedicated_subnet" {
  security_group_id = openstack_networking_secgroup_v2.shared_postgresql_access.id
  description       = "Allow connections to port 5432 from the 192.168.2.0/22 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = var.shared_postgresql_server_data.network_cidr
}

# Block device
resource "openstack_blockstorage_volume_v3" "shared_postgresql_data_vol" {
  name = var.shared_postgresql_server_data.vol_data_name
  size = var.shared_postgresql_server_data.vol_data_size
}

# Instance
resource "openstack_compute_instance_v2" "shared_postgresql_server" {
  name                    = var.shared_postgresql_server_data.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.shared_postgresql_server_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name, openstack_networking_secgroup_v2.shared_postgresql_access.name]
  block_device {
    uuid                  = var.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = var.main_private_network.name
  }
  network {
    name        = var.shared_postgresql_server_data.network_name
    fixed_ip_v4 = var.shared_postgresql_server_data.server_ip
  }

  user_data = file("${var.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}

resource "openstack_compute_volume_attach_v2" "shared_postgresql_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.shared_postgresql_server.id
  volume_id   = openstack_blockstorage_volume_v3.shared_postgresql_data_vol.id
  device      = var.shared_postgresql_server_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.shared_postgresql_server]
}
