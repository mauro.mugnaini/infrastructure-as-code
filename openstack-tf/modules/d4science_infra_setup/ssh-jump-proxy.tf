# VM used as jump proxy. A floating IP is required
resource "openstack_compute_instance_v2" "ssh_jump_proxy" {
  name                    = var.ssh_jump_proxy.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.ssh_jump_proxy.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name, openstack_networking_secgroup_v2.access_to_the_jump_proxy.name]
  block_device {
    uuid                  = var.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 30
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = var.main_private_network.name
    fixed_ip_v4 = var.basic_services_ip.ssh_jump
  }
  user_data = file("${var.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}

# Floating IP and DNS record
resource "openstack_networking_floatingip_v2" "ssh_jump_proxy_ip" {
  pool = var.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "SSH Proxy Jump Server"
}

resource "openstack_compute_floatingip_associate_v2" "ssh_jump_proxy" {
  floating_ip = openstack_networking_floatingip_v2.ssh_jump_proxy_ip.address
  instance_id = openstack_compute_instance_v2.ssh_jump_proxy.id
}

locals {
  ssh_recordset_name = "${var.ssh_jump_proxy.name}.${var.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "ssh_jump_proxy_recordset" {
  zone_id     = var.dns_zone_id
  name        = local.ssh_recordset_name
  description = "Public IP address of the SSH Proxy Jump server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.ssh_jump_proxy_ip.address]
}
