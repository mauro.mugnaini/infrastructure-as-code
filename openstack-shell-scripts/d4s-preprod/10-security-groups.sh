#!/usr/bin/env bash

. ./variables.sh
#
# Part of the security groups
#
# Remove the ingress rules from the default group
# openstack --os-cloud d4s-pre security group show default
rules_to_delete=
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules default | grep ingress | grep -v protocol | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi
openstack --os-cloud d4s-pre security group rule create \
  --description "Allow ICMP" --ingress --protocol icmp \
  --remote-ip 0.0.0.0/0 default
openstack --os-cloud d4s-pre security group rule create \
  --description "Prometheus node exporter" --ingress --protocol icmp \
  --dst-port "9100" \
  --remote-ip 10.1.32.0/22 default

# SSH access
openstack --os-cloud d4s-pre security group create \
  --description "Access to the SSH Proxy Jump server" \
  "Limited SSH access"
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules "Limited SSH access" | grep egress | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from S2I2S vpn 1" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.28.10/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from S2I2S vpn 2" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.28.11/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from D4Science VPN 1" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.27/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from D4Science VPN 2" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.49/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from shell.d4science.org" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.95/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "SSH from the InfraScience Network (because masquerade on the Xen hypervisors)" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.0/23 "Limited SSH access"
# Limited HTTPS access
openstack --os-cloud d4s-pre security group create \
  --description "Limited HTTPs and public HTTP. For servers with public IP addresses that can be accessed from our VPN only" \
  "Limited HTTPS access"
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules "Limited HTTPS access" | grep egress | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi

# HAPROXY, Prometheus, shell-jump: create VM with a fixed IP address so that we can statically define the security groups
# HAPROXY L7 security group id: 20ff5149-54d6-49b4-b7e4-31fef6f08b3f

dest_ports="8880 9999"
for port in $dest_ports ; do
  openstack --os-cloud ${os_infra} security group rule create --ingress --protocol tcp --dst-port "$port" --remote-ip 10.1.30.180/32 20ff5149-54d6-49b4-b7e4-31fef6f08b3f
  openstack --os-cloud ${os_infra} security group rule create --ingress --protocol tcp --dst-port "$port" --remote-ip 10.1.29.161/32 20ff5149-54d6-49b4-b7e4-31fef6f08b3f
done
